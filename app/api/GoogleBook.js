class GoogleBook {
    getByType(text, offset) {
        return new Promise((resolve, reject) =>
            fetchJSON(`https://www.googleapis.com/books/v1/volumes?q=${text}&startIndex=${offset * 10}`)
                .then(json => {
                    if (json) {
                        resolve(json)
                    } else {
                        reject(null)
                    }
                })
                .catch(err => reject(err))
        )
    }

    getBookById(id) {
        return new Promise((resolve, reject) =>
            fetchJSON(`https://www.googleapis.com/books/v1/volumes/${id}`)
                .then(json => {
                    if (json) {
                        resolve(json)
                    } else {
                        reject(null)
                    }
                })
                .catch(err => reject(err)))
    }
}