class Model {
    constructor() {
        this.api = new GoogleBook();
    }

    getByType(text, offset) {
        return this.api.getByType(text, offset);
    }

    getBookById(id) {
        return this.api.getBookById(id);
    }
}