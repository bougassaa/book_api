class BaseController {
    constructor() {
        this.setBackButtonView('index')
        this.model = new Model()
    }
    toast(msg) {
        // todo : change to Bootstrap
        //M.toast({html: msg, classes: 'rounded'})
    }
    displayServiceError() {
        // todo : change to Bootstrap
        //this.toast('Service injoignable ou problème réseau')
    }
    getModal(selector) {
        // todo : change to Bootstrap
        //return M.Modal.getInstance($(selector))
    }
    setBackButtonView(view) {
        window.onpopstate = function() {
            navigate(view)
        }; history.pushState({}, '');
    }
}
