class IndexController extends BaseController {
    constructor() {
        super();
        this.paginationOffset = 0;
        this.totalItems = 0;
        this.text = "";
    }

    async searchBook() {
        const input = $("#textSearch");

        if(input.value.length > 0 && input.value != this.text) {
            this.paginationOffset = 0;
            this.totalItems = 0;
        }

        this.text = input.value.length > 0 ? input.value : this.text;

        input.value = "";

        if(this.text.length > 0) {
            let json = await this.model.getByType(this.text, this.paginationOffset);

            this.totalItems = this.totalItems > json.totalItems ? this.totalItems : json.totalItems;

            $("#totalItems").innerText = this.totalItems > 0 ? `${this.totalItems} livres trouvés.` : "Aucun livre trouvé.";

            let bodyContent = "";

            if(this.totalItems > 0 && json.totalItems > 0) {
                $("#dataTable").style.display = "table";

                for (let item of json.items){
                    bodyContent += `<tr>
                                        <td><a href="javascript:void(0)" onclick="indexController.createModalForBook('${item.id}')">${item.volumeInfo.title}</a></td>
                                        <td>${item.searchInfo == undefined ? "N/A" : item.searchInfo.textSnippet}</td>
                                        <td>${item.volumeInfo.publishedDate == undefined ? "N/A" : item.volumeInfo.publishedDate.split('T')[0]}</td>
                                    </tr>`;
                }
            } else {
                $("#dataTable").style.display = "none";
                $(".next").classList.add('disabled');
            }

            $("#dataTable tbody").innerHTML = bodyContent;

            this.managePaginationButton();
        }
    }

    async createModalForBook(id) {
        let json = await this.model.getBookById(id);

        $("#titleModal").innerHTML = json.volumeInfo.title;
        $("#imgModal").src = json.volumeInfo.imageLinks != undefined ? json.volumeInfo.imageLinks.thumbnail : "https://static.thenounproject.com/png/140281-200.png";
        jQuery("#bookModal").modal('show');
    }

    paginationNext() {
        this.paginationOffset ++;
        this.searchBook();
        window.scrollTo(0, 0);
    }

    paginationPrevious() {
        this.paginationOffset = this.paginationOffset > 1 ? --this.paginationOffset : 0;
        this.searchBook();
        window.scrollTo(0, 0);
    }

    managePaginationButton() {
        if(this.totalItems > 10) {
            $(".pagination").classList.remove('d-none');
            $(".next").classList.remove('disabled');
        } else {
            $(".pagination").classList.add('d-none');
            $(".next").classList.add('disabled');
        }

        if(this.paginationOffset == 0){
            $(".previous").classList.add('disabled');
        } else {
            $(".previous").classList.remove('disabled');
        }

        if (this.paginationOffset * 10 > this.totalItems){
            $(".next").classList.add('disabled');
        } else {
            $(".next").classList.remove('disabled');
        }
    }
}

window.indexController = new IndexController();
